<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['cors']], function () {
    //Rutas a las que se permitirá acceso
	
});

Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/empresa', 'EmpresaController@index')->name('empresa');
Route::post('/empresa', 'EmpresaController@create')->name('empresa');
Route::get('/empresa/show/', 'EmpresaController@show')->name('empresa-mostrar');
Route::get('/clientes', 'ClientesController@index')->name('clientes');
Route::get('/cliente/{clienteId}/{empresaId}/', 'ClientesController@clienteId')->name('clientes-id');
Route::get('/configuracion', 'ConfiguracionController@index')->name('configuracion');
Route::get('/configuracion/addchat/1/', 'ConfiguracionController@addchat')->name('addchat');
Route::get('/perfil', 'HomeController@perfil')->name('perfil');

Route::post('/chatbot','HomeController@messageChatbot');

