function loadChat() {
	
	var ajax = new XMLHttpRequest();
	ajax.open("GET", "chatbot.html", false);
	ajax.send();
	console.log('load chatbot.js');
	document.body.innerHTML += ajax.responseText;
	document.getElementById("chatbot").addEventListener("click", myFunction);
	document.getElementById("id_close").addEventListener("click", closeChat);
}

function myFunction() {
	document.getElementById("chatbot").style.display = "none"; 
	document.getElementById("chatbot_chat").style.display = "block"; 
}

function closeChat() {
	document.getElementById("chatbot").style.display = "block"; 
	document.getElementById("chatbot_chat").style.display = "none"; 
}

loadChat();