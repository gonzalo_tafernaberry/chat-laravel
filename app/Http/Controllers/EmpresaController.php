<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Empresa;
use Auth;
use Session;

class EmpresaController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	
	
	public function index()
    {
        return view('empresa-create');
    }
	
	
	protected function create(Request $request)
    {
		$validator = Validator::make($request->all(), [
			'nombre' => ['required', 'string', 'max:255'],
			'direccion' => ['required', 'string', 'max:255'],
			'telefono' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'sitio_web' => ['required', 'string', 'max:255'],
			'imagen' => ['required', 'string', 'max:255'],
		]);
        
		if (false){//$validator->fails()) {
			Session::flash('error', $validator->messages()->first());
			return redirect()->back()->withInput();
		}
		
		if (Auth::check())
		{
			$save = Empresa::create([
				'user_id' => Auth::user()->id,
				'nombre' => $request['nombre'],
				'direccion' => $request['direccion'],
				'telefono' => $request['telefono'],
				'email' => $request['email'],
				'sitio_web' => $request['sitio_web'],
				'imagen' => $request['imagen'],
			]);			
		}
        
		
		return redirect('/home');
    }

    public function show(Request $request)
    {
    	$user_id = Auth::user()->id;
    	$empresa = DB::table('empresas')
			->where('user_id', $user_id)
			->get()
			->toArray();
    	return view('empresa', ['empresa' => $empresa]);
    }
}
