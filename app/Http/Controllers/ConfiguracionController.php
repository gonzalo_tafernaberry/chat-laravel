<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Http\Request;

class ConfiguracionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	
	public function index()
    {
        return view('configuracion');
    }

    public function addchat()
    {
        $user_id = Auth::user()->id;
        $empresa = DB::table('empresas')
            ->where('user_id', $user_id)
            ->get()
            ->toArray();
        return view('addchat', ['empresa' => $empresa]);
    }
}
