<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Chat;
use Response;
use Illuminate\Support\Facades\DB;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	public $count = 0;

	
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$token = openssl_random_pseudo_bytes(16);
		$token = bin2hex($token);
        return view('home', ['token' => $token]);
    }


    public function perfil()
	{
		$user_id = Auth::user()->id;
		$perfil = DB::table('users')
			->where('id', $user_id)
			->get();
		return view('perfil', ['perfil' => $perfil]);
	}
	
	/**
	 *
	*/
	public function mensajeChatbot($count)
	{
		$msg = array(
			'Será un placer asistirte, cuál es tu nombre?',
			'Yo me encargo de ponerte en contacto con un asesor para que responda tu consulta. <p>Cual es tu email?</p>',
			'Podría pedirte además tu teléfono completo?',
			'Perfecto!. Con la información que me has pasado un asesor se comunicará contigo a la brevedad. Quieres dejarme alguna consulta adicional?',
			'Bien, me contacto con el asesor y le doy toda esta información.',
			'Muchas gracias por comunicarte!'
		);
		
		return $msg[$count];
		
	}
	
	public function existeEmpresa($url)
	{
		$user_id = Auth::user()->id;
		$existeEmpresa = DB::table('empresas')
			->where('user_id', $user_id)
			->get();
		if ($existeEmpresa->count() > 0){
			$empresa_id = $existeEmpresa[0]->id;
			return $empresa_id;
		} else {
			return false;
		}
	}
	
	public function messageChatbot(Request $request)
    {
		
		if (!$request->post('mensage_usuario')){
			$data = array();
			$data['mensage_usuario'] = "Parameter: 'message_usuario' is required";
			return Response::json($data);
		}
		$mu = $request->post('mensage_usuario');
		$token = $request->post('token');
		$url = $request->post('url');
		
		if (!$token){
			$token = openssl_random_pseudo_bytes(16);
			$token = bin2hex($token);			
		}
		$id_empresa = $this->existeEmpresa($url);
		if (!$id_empresa){
			$data = array();
			$data['message'] = 'Empresa no existe';
			return Response::json($data);
		}
		error_log($request->post('token'));
        //return view('home');
		$mc = $this->mensajeChatbot($request->post('count'));
		$this->count += 1;
		
		$chat = new Chat;
		$chat->url = $url;
		$chat->token = $token;
		$chat->cliente = $mu;
		$chat->chatbot = $mc;
		$chat->empresa_id = $id_empresa;
		$chat->created_at = now();
		$chat->updated_at = now();
		$chat->save();
		
		$data = array();
		$data['count'] = $request->post('count');
		$data['status'] = 'success';
		$data['token'] = $token;
		$data['mensage_usuario'] = $mu;
		$data['mensage_chatbot'] = $mc;
		//return response()->json(['name' => 'Abigail', 'state' => 'CA']);
		return Response::json($data);
    }
}
