<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class ClientesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	/**
	*
	*/
	public function index()
    {
		$user_id = Auth::user()->id;
		
		$empresas = DB::table('empresas')
			->select('id')
			->where('user_id', $user_id)
			->get()
			->toArray();
		//dd(['0'=>['a'=>'b'],'1'=>2]);
		//var_dump($empresas[0]->id);
		//dd($empresas);
		$array_empresas = array();
		$count = 0;
		foreach ($empresas as $key => $value){
			//error_log($value);
			array_push($array_empresas, $value->id);
			$count += $count;
		} 
		//dd($array_empresas);
		
		$clientes = DB::table('chats')
			->where('empresa_id', $array_empresas)
			->groupBy('token')
			->orderBy('created_at', 'DESC')
			->get();
		
        return view('clientes', ['clientes' => $clientes]);
    }

    public function clienteId($clienteId, $empresaId)
    {
		$user_id = Auth::user()->id;
		
		$chats = DB::table('chats')
			->where('token', $clienteId)
			->where('empresa_id', $empresaId)
			->orderBy('created_at', 'ASC')
			->get()
			->toArray();
		
        return view('chats', ['chats' => $chats]);
    }
}
