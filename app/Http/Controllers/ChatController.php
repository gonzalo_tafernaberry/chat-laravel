<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function store(Request $request)
    {
        $article = Article::create($request->all());

        return response()->json($article, 201);
    }
}
