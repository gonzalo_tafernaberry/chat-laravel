@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Perfil') }}</div>
                <div class="card-body">
                    <div class="row justify-content-left">
                    @foreach ($perfil as $perf)
                        <div class="col-md-12 div_clientes">
                            <img id="blah" src="{{ $perf->image }}" alt="" class="img-responsive" />
                            <p>Nombre: {{ ucfirst($perf->name) }}</p>
                            <p>Apellido: {{ ucfirst($perf->surename) }}</p>
                            <p>Email: {{ $perf->email }}</p>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
