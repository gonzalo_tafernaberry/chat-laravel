@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Clientes</div>

                <div class="card-body">
                    <!--@if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
					-->
					<p>&nbsp;</p>
					<div class="row justify-content-left">
					@foreach ($clientes as $cliente)
                        <a href="/cliente/{{$cliente->token}}/{{$cliente->empresa_id}}/">
    						<div class="col-md-3 div_clientes">
    							<img id="blah" src="/img/user-icon3.png" alt="" class="img-responsive" />
    							<p>{{ \Carbon\Carbon::parse($cliente->created_at)->format('d-m-Y') }}</p>
    							<p>{{ \Carbon\Carbon::parse($cliente->created_at)->format('H:i:s') }}</p>
    						</div>
                        </a>
					@endforeach
					</div>
                    <canvas id="canvas-basic"></canvas>  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
