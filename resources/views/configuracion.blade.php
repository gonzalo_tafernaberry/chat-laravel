@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">ChatBot</div>

                <div class="card-body">
                    <!--@if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
					-->

                    <img src="/img/chat.png" alt="chat" style="width: 5rem;">
					<p>
						<a href="{{ route('addchat') }}">Agregar chat</a>
					</p>
                    <canvas id="canvas-basic"></canvas>  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
