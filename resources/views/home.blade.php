@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">ChatBot</div>

                <div class="card-body">
                    <!--@if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
					-->

                    Haga click en el chat abajo a la derecha  
					<p>{{$token}}</p>
                    <canvas id="canvas-basic"></canvas>  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
