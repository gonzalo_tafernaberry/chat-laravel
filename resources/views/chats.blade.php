@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Chat Cliente</div>
                <div class="card-body">
					<div class="row justify-content-left">
					@foreach ($chats as $chat)
						<div class="col-md-12 div_clientes">
							<p>{{$chat->cliente}}</p>
							<p>{{$chat->chatbot}}</p>
						</div>
					@endforeach
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
