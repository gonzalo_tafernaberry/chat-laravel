@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Empresa') }}</div>
                <div class="card-body">
                    <div class="row justify-content-left">
                    @foreach ($empresa as $emp)
                        <div class="col-md-12 div_clientes">
                            <img id="blah" src="{{ $emp->imagen }}" alt="" class="img-responsive" />
                            <p>Nombre: {{ $emp->nombre }}</p>
                            <p>Sitio Web: <a href="{{ $emp->sitio_web }}">{{ $emp->sitio_web }}</a></p>
                            <p>Dirección: {{ $emp->direccion }}</p>
                            <p>Telefóno: {{ $emp->telefono }}</p>
                            <p>Creado: {{ $emp->created_at }}</p>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
